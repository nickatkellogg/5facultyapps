# 5FacultyApps

GitPitch presentation on 5 apps developed for Kellogg Faculty.

[Presentation Link](https://gitpitch.com/nickatkellogg/5facultyapps/master?grs=bitbucket&t=black#/)

Bitly link: [http://bit.ly/5facultyapps](http://bit.ly/5facultyapps)

### Who do I talk to? ###

* Repo owner: Nicholas Bennett [nicholas.bennett@kellogg.northwestern.edu](mailto:nicholas.bennett@kellogg.northwestern.edu)
# 5FacultyApps

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/james-hammond-368466.jpg&size=contain

<!-- Photographer Credit -->
<!-- <a style="position:fixed !important;bottom:10px;left:0px;background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@jameshammond7?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from James Hammond"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">James Hammond</span></a> -->

Note:
LISTENING
In the course of working with Kellogg faculty, I listen closely to what they say they want, and occasionally
I chance upon something I can actually help them with. I'm ambitious to help and to make things, but I know
my limitations.

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/max-oh-484288.jpg&size=contain

<!-- Photographer Credit -->
<!-- <a style="position:fixed !important;bottom:10px;left:0px;background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@maxoh?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Max Oh"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Max Oh</span></a> -->

Note:
BUT BUSY
This is what it feels like in support sometimes. Very busy, lots of interruptions, many short transactions,
and impatient customers.


---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/noel-nichols-443895.jpg&size=contain

<!-- Photographer Credit -->
<!-- <a style="position:fixed !important;bottom:10px;left:0px;background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@noel_nichols?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Noel Nichols"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Noel Nichols</span></a> -->

Note:
JUGGLING ACT
Working on development projects typically takes a lot of concentration, like riding a bike. Doing that in a support office is like
riding a unicycle, on an off-road track.

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/veri-ivanova-17904.jpg&size=contain

<!-- Photographer Credit -->
<!-- <a style="position:fixed !important;bottom:10px;left:0px;background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@veri_ivanova?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Veri Ivanova"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Veri Ivanova</span></a> -->

Note:
NO TIME
Basically, I don't have a lot of time to work on these things.

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/caleb-woods-269348.jpg&size=contain

<!-- Photographer Credit -->
<!-- <a style="position:fixed !important;bottom:10px;left:0px;background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@caleb_woods?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Caleb Woods"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Caleb Woods</span></a> -->

Note:
PLAYING
This is about the level of what I'm able to do with code. I play with it and incidentally build things that work.

---

### Version 0.1

> I have to say, "OK. You know software version numbers? Mac OS version 10.4? 10.5? What you just described is
> version infinity. That's everything it will ever do in the future. First focus on launching version 0.1."

Derek Sivers, [Version Infinity](https://sivers.org/infinity)

Note:
I have the chance to figure out what Version 0.1 is going to be, and do that. If it doesn't end up getting used
very much, no big loss. If it outgrows its britches, I figure that's a good problem to have and I can find
someone to take it to the next level. That's YOU, the rest of KIS.

---

### Boring, Working Things

> "The question you must ask yourself before making any tech decisions about a new project is
> "What's the most boring thing that could possibly work?""

Sean Fioretti, [planningforaliens.com](https://www.planningforaliens.com/)

Note:
This fits into Version 0.1. The boring, working thing might just be the last version of it, at least for a very
long time. It might be enough.

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/ashim-d-silva-95242.jpg&size=contain

<!-- Photographer Credit -->
<!-- <a style="position:fixed !important;bottom:10px;left:0px;background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@randomlies?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Ashim D’Silva"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Ashim D’Silva</span></a> -->

Note:
(walk signal button on a road)

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/adam-sherez-228582.jpg&size=contain

<!-- Photographer Credit -->
<!-- <a style="position:fixed !important;bottom:10px;left:0px;background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px;" href="https://unsplash.com/@mr_sherez?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Adam Sherez"><span style="display:inline-block;padding:2px 3px;"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-1px;fill:white;" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M20.8 18.1c0 2.7-2.2 4.8-4.8 4.8s-4.8-2.1-4.8-4.8c0-2.7 2.2-4.8 4.8-4.8 2.7.1 4.8 2.2 4.8 4.8zm11.2-7.4v14.9c0 2.3-1.9 4.3-4.3 4.3h-23.4c-2.4 0-4.3-1.9-4.3-4.3v-15c0-2.3 1.9-4.3 4.3-4.3h3.7l.8-2.3c.4-1.1 1.7-2 2.9-2h8.6c1.2 0 2.5.9 2.9 2l.8 2.4h3.7c2.4 0 4.3 1.9 4.3 4.3zm-8.6 7.5c0-4.1-3.3-7.5-7.5-7.5-4.1 0-7.5 3.4-7.5 7.5s3.3 7.5 7.5 7.5c4.2-.1 7.5-3.4 7.5-7.5z"></path></svg></span><span style="display:inline-block;padding:2px 3px;">Adam Sherez</span></a> -->

Note:
(hammers hanging on the wall)

---

# 5FacultyApps

Here are 5 Version 0.1 apps that I've helped build which satisfy needs of Kellogg Faculty.

---

### 1. [Faculty Search App](http://kisfacultydocs.kellogg.northwestern.edu/directory/faculty/)

![Faculty Search Demo](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/videos/faculty_search_demo.mp4)

Note:
(animation of faculty search app)
This is an app we use in support to put faces to names of Kellogg faculty, which is both a reference
for existing staff and a tool we use to more quickly onboard new staff. With quick links for each
faculty member to search for them at Amazon, YouTube, and Google Scholar, I can more quickly learn
about the person I'm assisting.

I wrote a scraper of the faculty directory using the main programming language I know, Python, extracting
the information to a JSON file.

I hacked step 5 of the Angular JS tutorial (that's as far as I got) to display the data from that JSON
file with a live search field indexing all fields.

+++

Source code is in a [private Bitbucket git repository](https://bitbucket.org/nickatkellogg/kellogg_faculty_directory/src).

![Faculty Search Bitbucket git repository](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/faculty_search-bitbucket.png)

---

### 2. [Kellogg Board Fellows Job Matcher](http://ec2-34-215-97-127.us-west-2.compute.amazonaws.com:8000/)

![Kellogg Board Fellows Job Matcher Initial Screen](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/kbf-jobmatcher-initial.png)

Note:
(image of the KBF Job Matcher app)
The staff of the Kellogg Board Fellows program have been using this app for the past 5 years to fairly
and optimally match their 50 students to 60 internships individually ranked by preference. It took over
a laborious and error-prone manual process. It looks like 1995 Web but it just works.

This was my first web app, set up on Google App Engine with Python because that was the easiest thing for
me to wrap my head around and the hosting was free. I've since minimally ported it to another Python
web application framework and moved it to an AWS EC2 Micro instance, updated it to work with the newer
output format of Qualtrics (where the preferences of the students are gathered), but otherwise it still
looks like 1995.

---

![Kellogg Board Fellows Job Matcher Results Screen](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/kbf-jobmatcher-results.png)

Note:
(image of the KBF Job Matcher app)
The staff of the Kellogg Board Fellows program have been using this app for the past 5 years to fairly
and optimally match their 50 students to 60 internships individually ranked by preference. It took over
a laborious and error-prone manual process. It looks like 1995 Web but it just works.

This was my first web app, set up on Google App Engine with Python because that was the easiest thing for
me to wrap my head around and the hosting was free. I've since minimally ported it to another Python
web application framework and moved it to an AWS EC2 Micro instance, updated it to work with the newer
output format of Qualtrics (where the preferences of the students are gathered), but otherwise it still
looks like 1995.

+++

Source code is in a [public Bitbucket git repository](https://bitbucket.org/nickatkellogg/kjobmatcher/src).

![Kellogg Board Fellows Job Matcher Bitbucket git repository](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/kbf-jobmatcher-bitbucket.png)

---

### 3. [RStudio + Shiny + MySQL Server](http://datascience.kellogg.northwestern.edu/)

![RStudio Server](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/rstudio-server-interface.png)

Note:
With the aid of Syed and Paul Sliwa, we set up a server being used by the Data Science Club, sponsored by
Bob McDonald, which provides a consistent RStudio environment with a web interface, allows publishing of
R/Shiny apps to a public view, and gives the students experience with accessing data in a MySQL database.

This has significantly cut down on the time needed in Bob McDonald's and the Data Science Club's R
tutorials to get students set up with a common environment. Using open-source versions of the software,
with excellent free setup guides, we set this up with zero licensing costs.

+++

Shell scripts for setting up these servers are available in a [public Github git repository](https://github.com/tothebeat/vagrant-rstudio-shiny-webmin).

![RStudio Server Github git repository](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/rstudio-server-github.png)

---

### 4. [Qualtrics JS](https://github.com/tothebeat/qualtrics-js/tree/master/lookup-values-once)

![Qualtrics JS POC](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/qualtrics-js-poc.png)

Note:
In conversation with Brett Gordon about WordPress support, he mentioned offhand a class exercise
he's been running in his Retail Analytics class that is labor intensive to administer, and he
idly wondered if that was something we could help him with.

Tim Richardson and I gathered requirements from Brett, and little did we know that Rick Salisbury
had also been thinking about this. We investigated both Qualtrics and Trinket.io (browser-based
Python applets), based on prior work I had done in 2013 for Brian Sternthal of custom JavaScript
applets in Qualtrics. Between the basic Qualtrics documentation on their JavaScript API, my prior
work, and other peoples' work I found in Github searches, I was able to piece together a working
POC along with extensive documentation on setting it up.

I've helped him customize this for the real data, and he's planning on using it in the Spring
quarter and potentially in the Winter quarter (class is being taught by a different professor,
Anna Tuchman).

This has helped him increase engagement with students and eliminate the need for the teaching
faculty member to be available at the time the assignment is due. Previously, the students
would form groups and submit data to Brett, he would copy data into an Excel file and run a
calculation, then copy data out of the Excel spreadsheet and send back to the student group.
Due to the automated nature of this, he's able to offer this assignment to individual students
rather than just groups, he doesn't have to attend to the data computation, and the students
get immediate answers.

+++

All documentation is in a [public Github git repository](https://github.com/tothebeat/qualtrics-js/tree/master/lookup-values-once).

![Qualtrics JS Github git repository](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/qualtrics-js-github.png)

--- 

### 5. [WordPress Consultation on watchonlinetoday.com](https://www.watchonlinetoday.com/)

![watchonlinetoday.com login screen](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/watchonlinetoday-login.png)

Note:
Brett Gordon is running a research study that involves showing videos in special sequences to participants,
and he's doing this through the web using a website called watchonlinetoday.com. Although he contracted a
third-party contractor from Upwork.com to build the site, he was mystified as to how to get the proper
hosting.

I've helped him work with the developer, understanding that it's a WordPress-based web app, and I've
recommended and set up managed WordPress hosting with his preferred provider. I've found workarounds
for technical limitations of the hosting, when Brett's and his developer's needs are greater than
what's provided. I'm confident he's been able to get set up with the site much more quickly, with greater
success, and at a lower cost with our help.

There's potential in the future to consult on finding other third-party developers, take over development
or maintenance, and troubleshooting with the present developer.

+++

Backup of the site is in a [private Bitbucket git repository](https://bitbucket.org/nickatkellogg/watchonlinetoday.com/src).

![Bitbucket git repository](https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/watchonlinetoday-bitbucket.png)

---

# Hey, you!
# Build the future.

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/lego-astronaut.jpg&size=contain

Note:
(child building an astronaut suit out of Lego bricks from the inside)
I wanted to show you a bunch of Version 0.1's so you could think about what the next version might be,
and to think about what else we might build if we listen closely for opportunities.

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/lego-firefighter.jpg&size=contain

Note:
(child building a firefighter suit out of Lego bricks from the inside)
I wanted to show you a bunch of Version 0.1's so you could think about what the next version might be,
and to think about what else we might build if we listen closely for opportunities.

---?image=https://s3-us-west-2.amazonaws.com/5facultyappskellogg/assets/images/lego-rockstar.jpg&size=contain

Note:
(child building a rockstar suit out of Lego bricks from the inside)
I wanted to show you a bunch of Version 0.1's so you could think about what the next version might be,
and to think about what else we might build if we listen closely for opportunities.
